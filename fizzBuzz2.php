<?php


class FizzBuzz {
    private $maxNumber;
    private $factorReplacementArr = [];
    
    public function __construct($number){
        $this->setMaxNumber($number);
    }
    
    public function getMaxNumber(){
        return $this->maxNumber;
    }
    
    public function setMaxNumber($number){
        $this->maxNumber = $number;
    }
    
    public function getFactors(){
        return $this->factorReplacementArr;
    }
    
    public function addFactor($number,$str){
        if(!array_key_exists($number,$this->factorReplacementArr)){
            $this->factorReplacementArr[$number] = $str;
        }
    }
    
    public function removeFactor($number){
        if(array_key_exists($number,$this->factorReplacementArr)){
            unset($this->factorReplacementArr[$number]);
        }
    }
    
    function hasFactors($number){
        $str = '';

        foreach($this->factorReplacementArr as $factor => $factorStr){
            if($number%$factor === 0){
                $str = $str.$factorStr;
            }
        }

        if($str === ''){
            return $number;
        } else {
            return $str;
        }
    }

    
    public function outputNumbers(){
        for($i=1; $i<=$this->getMaxNumber(); $i++){
            echo($this->hasFactors($i,$this->factorReplacementArr).', ');
            if($i%30 === 0){
                echo('<br>');
            }
        }
    }
}

$fizzBuzz = new FizzBuzz(1000);
$fizzBuzz->addFactor(3,'Fizz');
$fizzBuzz->addFactor(5,'Buzz');

echo('<br>');
var_dump($fizzBuzz->getFactors());
echo('<br>');

$fizzBuzz->outputNumbers();


