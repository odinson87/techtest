<?php

class Location{
    private $name;
    private $lat;
    private $lon;
    private $currentDistance;
    
    public function __construct($jsonStr){
        $arr = json_decode($jsonStr);
        $this->name = $arr->name;
        $this->lat = (float) $arr->lat;
        $this->lon = (float) $arr->lon;
    }

    public function getName(){
        return $this->name;
    }
    
    public function getLat(){
        return $this->lat;
    }
    
    public function getLon(){
        return $this->lon;
    }
    
    public function getLatLon(){
        return $arr = [$this->lat,$this->lon];
    }
    
    public function getObj(){
        $arr = [
            "name" => $this->name,
            "lat" => $this->lat,
            "lon" => $this->lon,
            "currentDistance" => $this->currentDistance
        ];
        return $arr;
    }
    
    public function setDistance($distance){
        $this->currentDistance = $distance;
    }
    
    public function getDistance(){
        return $this->currentDistance;
    }

}