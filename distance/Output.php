<?php

class Output{
    
    private $locationsPath;
    private $locationsRaw;
    private $locations = [];
    private $destination;
    private $clients = [];
    private $distances = [];
    private $testMode; //boolean
    private $validUnits = ['km','miles'];
    private $invites = [];
    
    
    public function __construct($jsonPath,$destination,$test = false){
        if(preg_match('/\.json$/',$jsonPath) > 0 && is_bool($test)){
            $this->locationsPath = $jsonPath;
            $this->testMode = $test;
            
            $this->setLocations();
            $this->setDestination($destination);
            $this->setClients();
            
        }
    }
    
    private function setLocations(){
        $this->locationsRaw = file_get_contents($this->locationsPath);
        
        if($this->locationsRaw){
        
            if($this->testMode === true){
                echo 'Raw json string from file :<br>';
                echo $this->locationsRaw;
                echo('<br>');                
            }
            
            $this->locations = json_decode($this->locationsRaw);
        
            if($this->testMode === true){
                echo '<br>Decoded json string from file :';
                var_dump($this->locations);
                echo('<br>');
            }
        }
    }
    
    private function setDestination($desName){
        
        foreach($this->locations as $l){
            $jsonStr = json_encode($l);
            
            if($l->name === $desName){
                $this->destination = new Location($jsonStr);                
            
                if($this->testMode === true){               
                    echo('<br>Destination JSON : ');
                    echo($jsonStr);
                    echo('<br>');
                }
            }
        }
        
        if($this->testMode === true){               
            echo '<br>Destination Obj : <br>';
            var_dump($this->destination);
            echo '<br><br>';
        }
    }
    
    private function setClients(){
        
        $this->clients = [];
        
        foreach($this->locations as $l){
            $jsonStr = json_encode($l);
            
            if($l->name !== $this->destination->getName()){
                $client = new Location($jsonStr);
                
                if($this->testMode === true){
                    echo('<br>Client JSON : ');
                    echo($jsonStr);
                    echo('<br>');
                }
                
                $this->clients[] = $client;
            }
            
        }
        
        if($this->testMode === true){
            echo '<br>Clients Array : <br>';
            var_dump($this->clients);
            echo '<br><br>';
        }
    }
    
    public function invite($maxDistance,$unit){
        
        foreach($this->clients as $client){
            
            if(in_array(strtolower($unit),$this->validUnits)){
                $distance = new GreatCircleDistance($client->getLatLon(),$this->destination->getLatLon());
                $client->setDistance($distance->getDistance($unit,'ant'));
                
                if($this->testMode === true){
                    echo( $client->getName().' - Haversine Distance Km : '.$distance->getDistance('Km','hav').'<br>');
                    echo( $client->getName().' - Haversine Distance Miles : '.$distance->getDistance('Miles','hav').'<br>');
                    echo( $client->getName().' - Antipodal Distance Km : '.$distance->getDistance('KM','ant').'<br>');
                    echo( $client->getName().' - Antiodal Distance Miles : '.$distance->getDistance('MILES','ant').'<br>');
                    echo('<br>');
                }
            
                if($client->getDistance() < $maxDistance){
                    $this->invites[] = $client->getObj();

                    if($this->testMode === true){
                        echo $client->getName().' is within '.$maxDistance.' '.$unit;
                        var_dump($client);
                        echo '<br><br>';
                    }
                } else {
                    if($this->testMode === true){
                        echo $client->getName().' is too far away';
                        var_dump($client);
                        echo '<br><br>';
                    }
                }
                
            } else {
                echo 'Invalid Distance Unit : should be KM or Miles';
            }
        }
        
        if($this->testMode === true){
            echo('Invite List: <br>');
            var_dump($this->invites);
        }
        echo json_encode($this->invites);
    }
    
}