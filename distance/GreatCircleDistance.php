<?php

class GreatCircleDistance{
    
    const MEANEARTHRAD = 6371; // in KM
    const KM_MILE = 0.621371;
    private $arrPosName = [0=>'Latitude',1=>'Longitude'];
    private $latLon1;
    private $latLon2;
    private $havDistance;
    private $antDistance;
    
    public function __construct(array $latLonA, array $latLonB){
        if( $this->validateLatLon($latLonA) && $this->validateLatLon($latLonB) ){    
            $this->latLon1 = $latLonA;
            $this->latLon2 = $latLonB;
            $this->havDistance = $this->calcDistance('haversine');
            $this->antDistance = $this->calcDistance('antipodal');
        }
    }
    
    private function validateLatLon($latLon){
        if(count($latLon) === 2) {
            foreach($latLon as $k => $v){
                if( !is_float($v) ){
                    echo 'Invalid : '.$arrPosName[$k].' value is not a float';
                    return false;
                }
                return true;
            }
        } else {
            echo 'Invalid latLon : Be sure to include 2 float values for each latLong Array<br>';
            var_dump($lanLon);
            return false;
        }
    }
    
    private function calcDistance($type){
        $lat1 = deg2rad($this->latLon1[0]);
        $lat2 = deg2rad($this->latLon2[0]);
        $absLat = deg2rad($this->latLon2[0] - $this->latLon1[0]);
        $absLon = deg2rad($this->latLon2[1] - $this->latLon1[1]);
        
        if($type === 'haversine'){
            
            $haversine = sin($absLat/2) * sin($absLat/2) + cos($lat1) * cos($lat2) * sin($absLon/2) * sin($absLon/2);
            $a = 2 * atan2(sqrt($haversine),sqrt(1-$haversine));
            return self::MEANEARTHRAD * $a;
            
        } else if( $type === 'antipodal'){
            
            $a = cos($lat2) * sin($absLon);
            $b = cos($lat1) * sin($lat2) - sin($lat1) * cos($lat2) * cos($absLon);
            $dividend = sqrt($a*$a+$b*$b);
            $divisor = sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($absLon);
            $c = atan2($dividend,$divisor);
            return self::MEANEARTHRAD * $c;
            
        }
    }
    
    public function getDistance($units,$type){
        $dist;
        if($type === 'hav'){
            $dist = $this->havDistance;
        } else if($type === 'ant'){
            $dist = $this->antDistance;
        }
        
        if(strtolower($units) === 'km'){
            return $dist;
        } else if(strtolower($units) === 'miles'){
            return $dist * self::KM_MILE;
        }
    }
    
}