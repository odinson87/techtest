<?php

    require 'distance/Location.php';
    require 'distance/GreatCircleDistance.php';
    require 'distance/Output.php';
    
    if(isset($_POST['filename']) && isset($_POST['maxDistance']) && isset($_POST['unit']) && isset($_POST['destination']) ){
        $testMode = false;
        $filename = 'distance'.DIRECTORY_SEPARATOR.$_POST['filename'];
        $destinationName = $_POST['destination'];
        $maxDistance = $_POST['maxDistance'];
        $unit = $_POST['unit'];
    } else {
        $testMode = true;
        $filename = 'distance/locations.json';
        $destinationName = 'PayPlan';
        $maxDistance = 20;
        $unit = 'Miles';
    }
    
    $bbq = new Output($filename,$destinationName,$testMode);
    $bbq->invite($maxDistance,$unit);
    
    
    
    
    
    