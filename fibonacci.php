<?php

function nFibonacci($n){

    $fibArr = [0,1,1];
    $currentNum = 1;
    $currentPos = count($fibArr);
    
    
    if($n<=3){
        return $fibArr[$n-1];
    } else {
        
        while(count($fibArr) <$n){
            $currentNum = $fibArr[$currentPos-1] + $fibArr[$currentPos-2];
            $fibArr[] = $currentNum;
            $currentPos++;
        }
        var_dump($fibArr);
        return $fibArr[$n-1];
    }
}

$fibNum = 10;
echo 'Fibonacci Number '.$fibNum.' = '.nFibonacci(10);