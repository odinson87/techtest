<?php

function hasFactors($number,$factors){
    $str = '';

    foreach($factors as $factor => $factorStr){
        if($number%$factor === 0){
            $str = $str.$factorStr;
        }
    }

    if($str === ''){
        return $number;
    } else {
        return $str;
    }
}

$arr = [
    3=>"Fizz",
    5=>"Buzz"
];

for($i=1; $i<=1000; $i++){
    echo(hasFactors($i,$arr).', ');
    if($i%30 === 0){
        echo('<br>');
    }
}


